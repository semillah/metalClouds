import SwiftUI
import MetalKit

struct ContentView: View {
    @State private var hideStatusBar = true         // this is how we set the status bar state
    
    var body: some View {
        MetalView()
            .edgesIgnoringSafeArea(.all)
            .statusBar(hidden: hideStatusBar)                // hide status bar
            .persistentSystemOverlays(.hidden)
    }
}

struct MetalView: UIViewRepresentable {
    func makeUIView(context: Context) -> MTKView {
        let mtkView = MTKView()
        mtkView.device = MTLCreateSystemDefaultDevice()
        mtkView.delegate = context.coordinator
        mtkView.preferredFramesPerSecond = 120
        mtkView.isPaused = false
        return mtkView
    }
    
    func updateUIView(_ uiView: MTKView, context: Context) {
        // Perform any necessary updates to the MTKView when the view's state changes.
    }
    
    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }
    
    class Coordinator: NSObject, MTKViewDelegate {
        var parent: MetalView
        var device: MTLDevice!
        var commandQueue: MTLCommandQueue!
        var pipelineState: MTLRenderPipelineState!
        var pointBuffer: MTLBuffer!
        var uniformBuffer: MTLBuffer!
        var numberOfPoints: Int = 0
        var startTime: TimeInterval = 0
        var lastFrameTime: TimeInterval = 0
        
        struct Uniforms {
            var time: Float
            var strength: Float
            var frequency: Float
            var octaves: Int32
            var persistence: Float
            var phaseShift: Float
        }
        
        init(_ parent: MetalView) {
            self.parent = parent
            super.init()
            setupMetal()
            setupPoints()
            setupUniforms()
        }
        
        func setupMetal() {
            guard let device = MTLCreateSystemDefaultDevice() else {
                fatalError("Metal is not supported on this device")
            }
            self.device = device
            commandQueue = device.makeCommandQueue()
            
            let library = device.makeDefaultLibrary()
            let vertexFunction = library?.makeFunction(name: "vertex_main")
            let fragmentFunction = library?.makeFunction(name: "fragment_main")
            
            let pipelineDescriptor = MTLRenderPipelineDescriptor()
            pipelineDescriptor.vertexFunction = vertexFunction
            pipelineDescriptor.fragmentFunction = fragmentFunction
            pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm
            
            do {
                pipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
            } catch let error {
                fatalError("Failed to create pipeline state: \(error)")
            }
        }
        
        func setupPoints() {
            let gridSize: Int = 400
            let spacing: Float = 0.0055
            var points: [SIMD2<Float>] = []
            
            for i in 0..<gridSize {
                for j in 0..<gridSize {
                    let x = Float(i) * spacing - (spacing * Float(gridSize) / 2)
                    let y = Float(j) * spacing - (spacing * Float(gridSize) / 2)
                    points.append(SIMD2<Float>(x, y))
                }
            }
            
            numberOfPoints = points.count
            pointBuffer = device.makeBuffer(bytes: points, length: MemoryLayout<SIMD2<Float>>.size * numberOfPoints, options: [])
        }
        
        func setupUniforms() {
            var initialUniforms = Uniforms(time: 0, strength: 1.0, frequency: 1.0, octaves: 2, persistence: 0.5, phaseShift: 0.0)
            uniformBuffer = device.makeBuffer(bytes: &initialUniforms, length: MemoryLayout<Uniforms>.size, options: [])
        }
        
        func updateUniforms() {
            let currentTime = CACurrentMediaTime()
            if startTime == 0 {
                startTime = currentTime
            }
            
            let elapsedTime = Float(currentTime - startTime)
            
            // Scale down the time component to slow down the color change rate
            let scaledTime = elapsedTime * 0.1 // Adjust this scaling factor as needed
            
            var uniforms = Uniforms(time: scaledTime, strength: 0.1, frequency: 3.0, octaves: 8, persistence: 0.5, phaseShift: scaledTime)
            
            memcpy(uniformBuffer.contents(), &uniforms, MemoryLayout<Uniforms>.size)
        }
        
        
        func draw(in view: MTKView) {
            updateUniforms()
            
            guard let drawable = view.currentDrawable,
                  let pipelineState = pipelineState,
                  let commandBuffer = commandQueue.makeCommandBuffer(),
                  let renderPassDescriptor = view.currentRenderPassDescriptor,
                  let renderEncoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else {
                return
            }
            
            // Clear the view with a black background before drawing
            renderPassDescriptor.colorAttachments[0].loadAction = .clear
            renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0, 0, 0, 1)
            
            // Configure the pipeline and set the vertex buffer
            renderEncoder.setRenderPipelineState(pipelineState)
            renderEncoder.setVertexBuffer(pointBuffer, offset: 0, index: 0)
            renderEncoder.setVertexBuffer(uniformBuffer, offset: 0, index: 1)
            renderEncoder.setFragmentBuffer(uniformBuffer, offset: 0, index: 1)
            
            // Issue a draw call to render the points
            renderEncoder.drawPrimitives(type: .point, vertexStart: 0, vertexCount: numberOfPoints)
            
            // Finalize encoding and end the frame
            renderEncoder.endEncoding()
            
            // Present the drawable to the screen
            commandBuffer.present(drawable)
            commandBuffer.commit()
        }
        
        func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
            // Respond to drawable size change if necessary
        }
    }
}
