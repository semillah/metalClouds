//
//  metalCloudsApp.swift
//  metalClouds
//
//  Created by Raul on 1/13/24.
//

import SwiftUI

@main
struct metalCloudsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
